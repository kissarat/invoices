import {IndexRedirect, Router, Route, browserHistory} from 'react-router'
import {render} from 'react-dom'
import api from './api.jsx'
import React from 'react'
import App from './app.jsx'
import List from './list.jsx'
import InvoiceEdit from './invoice/edit.jsx'
import ProductList from './product/list.jsx'
import Edit from './edit.jsx'

const router = <Router history={browserHistory}>
  <Route path="/" component={App}>
    <IndexRedirect to="invoices/"/>
    <Route path="products/" component={ProductList}/>
    <Route path=":collection/" component={List}/>
    <Route path="invoices/create" component={InvoiceEdit}/>
    <Route path=":collection/create" component={Edit}/>
    <Route path="invoices/:id/edit" component={InvoiceEdit}/>
    <Route path=":collection/:id/edit" component={Edit}/>
  </Route>
</Router>

document.addEventListener('DOMContentLoaded', async function main() {
  await api.loadConfig()
  render(router, document.getElementById('app'))
})
