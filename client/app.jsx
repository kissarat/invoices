import {Link} from 'react-router'
import React from 'react'

const items = {
  Products: '/products/',
  Customers: '/customers/',
  Invoices: '/invoices/'
}

function menu() {
  const array = []
  for(const label in items) {
    array.push(<li key={label} className="nav-item"><Link className="nav-link" to={items[label]}>{label}</Link></li>)
  }
  return array
}

export default function App({children}) {
  return <main>
    <nav className="navbar navbar-default navbar-static-top">
      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                  aria-expanded="false" aria-controls="navbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"/>
            <span className="icon-bar"/>
            <span className="icon-bar"/>
          </button>
          <a className="navbar-brand" href="#">Invoice App</a>
        </div>
        <div id="navbar" className="navbar-collapse collapse">
          <ul className="nav navbar-nav">
            {menu()}
          </ul>
        </div>
      </div>
    </nav>

    <div className="container">
      {children}
    </div>
  </main>
}
