import React from 'react'
import api from './api.jsx'
import {Link} from 'react-router'
import AutoComponent from './auto.jsx'
import {isObject, size, omit} from 'lodash'

function getTags(object) {
  return Object.values(omit(object, 'createdAt', 'updatedAt', 'customer_id', 'discount', 'total'))
    .map(v => isObject(v) ? getTags(v) : v.toString().trim().replace(/\s+/g, ' ')).join(' ').toLocaleLowerCase()
}

export default class List extends AutoComponent {
  async load(props = this.props) {
    if (this.collectionName(props) !== this.state.collectionName) {
      this.setState({items: false})
      const items = await api.fetch(this.getURL(props))
      for(const item of items) {
        item.tags = getTags(item)
      }
      if (items instanceof Array) {
        this.setState({
          items,
          collectionName: this.collectionName(props)
        })
      }
      return items
    }
  }

  get fieldNames() {
    return Object.keys(this.model.fields).map(name => {
      const rex = /^(.*)_id$/.exec(name)
      return rex ? rex[1] : name
    })
  }

  headers() {
    const headers = this.fieldNames.map(name => <th key={name}>{name}</th>)
    headers.push(<th key="actions"/>)
    return headers
  }

  fields(item) {
    const fields = []
    for (const name of this.fieldNames) {
      const value = item[name]
      if (isObject(value)) {
        fields.push(<td key={name} className={name}>{value.name}</td>)
      }
      else {
        fields.push(<td key={name} className={name}>{value}</td>)
      }
    }
    if (this.isSelect) {
      const input = isObject(this.props.selected)
        ? <input
          type="number"
          value={this.props.selected[item.id] > 0 ? +this.props.selected[item.id] : ''}
          onChange={e => this.props.select(item, +e.target.value)}
        />
        : <input
          type="radio"
          checked={this.props.selected === item.id}
          onChange={() => this.props.select(item)}
        />
      fields.push(<td key="selection">{input}</td>)
    }
    else {
      fields.push(<td key="actions">
        <Link className="glyphicon glyphicon-pencil" to={`/${this.props.params.collection}/${item.id}/edit`}/>
      </td>)
    }
    return fields
  }

  onSelect(item) {
    return () => {
      if (this.isSelect) {
        this.props.select(item)
      }
    }
  }

  items() {
    const words = (this.state.search || '').trim().split(/\s+/)
      .map(w => w.toLowerCase())
    let items = this.state.items
    if (words.length > 0) {
      items = items.filter(item => words.every(w => item.tags.indexOf(w) >= 0))
    }
    return items.map(item => <tr
        key={item.id}
        onClick={this.onSelect(item)}
        className={(isObject(this.props.selected)
          ? this.props.selected[item.id] > 0
          : this.props.selected === item.id) ? 'selected' : ''}>
        {this.fields(item)}
      </tr>)
  }

  get isSelect() {
    return 'function' === typeof this.props.select
  }

  title() {
    if (!this.isSelect) {
      return <h1>{this.props.params.collection}</h1>
    }
  }

  isNoSelection(props = this.props) {
    return this.isSelect && (isObject(props.selected) ? size(props.selected) <= 0 : !(this.props.selected > 0))
  }

  table() {
    if (this.state.items instanceof Array) {
      if (this.state.items.length > 0) {
        return <table className="table">
          <thead>
          <tr>{this.headers()}</tr>
          </thead>
          <tbody>{this.items()}</tbody>
        </table>
      }
      return <div className="empty">Nothing</div>

    }
    else {
      return <div className="busy">Loading...</div>
    }
  }

  render() {
    return <div className={'page list ' + (this.isNoSelection() ? 'no-selection' : '')}>
      {this.title()}
      <Link className="create btn btn-primary" to={`/${this.collectionName()}/create`}>Create</Link>
      <input
        type="search"
        placeholder="Search"
        value={this.state.value}
        onKeyUp={e => this.setState({search: e.target.value})}/>
      {this.table()}
    </div>
  }
}
