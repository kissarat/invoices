import React from 'react'
import List from '../list.jsx'
import api from '../api.jsx'

export default class ProductList extends List {
  static collection = 'products'

  async load(props = this.props) {
    const products = await super.load(props)
    return api.loadPrices(products)
  }
}
