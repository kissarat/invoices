import api from '../api.jsx'
import Edit from '../edit.jsx'
import List from '../list.jsx'
import ProductList from '../product/list.jsx'
import React from 'react'
import {pick, clone, isObject, size} from 'lodash'

export default class InvoiceEdit extends Edit {
  static collection = 'invoices'

  componentWillMount() {
    this.products = {}
  }

  async load(props) {
    if (!this.state.items) {
      this.setState({items: {}})
      await api.loadPrices()
    }
    const invoice = await super.load(props)
    if (invoice && invoice.id > 0) {
      this.items = await api.fetch(`/api/invoices/${invoice.id}/items`)
      const items = {}
      for (const item of this.items) {
        if (item.quantity > 0) {
          items[item.id] = item.quantity
        }
      }
      this.setState({items})
    }
    else {
      this.items = []
    }
  }

  get omited() {
    return pick(this.model.fields, 'discount')
  }

  customer() {
    return <div className="form-group field-customer selection">
      <label>Customer</label>
      <List
        params={{collection: 'customers'}}
        selected={this.state.customer_id}
        select={customer => this.setState({customer_id: customer.id})}/>
    </div>
  }

  setQuantity = (product, n) => {
    n = +n
    const current = +this.state.items[product.id]
    if ((current || Number.isFinite(n)) && current !== n) {
      this.state.items[product.id] = n
      this.setState({products: this.state.items}, () => {
        if (!this.isNew()) {
          this.saveItem(product.id)
        }
      })
    }
  }

  itemList() {
    return <div className="form-group field-products selection">
      <label>Products</label>
      <ProductList
        selected={this.state.items}
        select={this.setQuantity}/>
    </div>
  }

  additional() {
    if (this.isNew() || this.state.customer_id > 0) {
      return <div className="additional">
        {this.customer()}
        {this.itemList()}
      </div>
    }
  }

  selectedItems() {
    const products = {}
    for (const id in this.state.items) {
      const n = this.state.items[id]
      if (n > 0) {
        products[id] = n
      }
    }
    return products
  }

  getItem(product_id) {
    return this.items.find(p => p.product_id === product_id)
  }

  alert() {
    if (this.isNew() && Object.values(this.selectedItems()).reduce((b, a) => a + b, 0) > 0) {
      return <div className="alert alert-warning" role="alert">
        Invoice items will be saved automatically after you create invoice.
        Items will immediately save when you edit an invoice
      </div>
    }
    return super.alert()
  }

  saveItem(product_id, id = this.props.params.id) {
    const quantity = +this.state.items[product_id]
    product_id = +product_id
    const product = this.getItem(product_id)
    if (quantity > 0) {
      const body = {
        product_id,
        quantity
      }
      if (product) {
        return api.fetch(`/api/invoices/${id}/items/` + product.id, {method: 'PUT', body})
      }
      else {
        return api.fetch(`/api/invoices/${id}/items`, {method: 'POST', body})
      }
    }
    else if (product) {
      return api.fetch(`/api/invoices/${id}/items/` + product.id, {method: 'DELETE'})
    }
  }

  getPersistent() {
    const data = super.getPersistent()
    data.total = this.getTotal()
    data.discount = +data.discount
    delete data.items
    return data
  }

  beforeSave() {
    if (this.state.customer_id > 0) {
      const discount = +this.state.discount
      if (isFinite(discount) && discount >= 0 && discount <= 100) {
        if (size(this.selectedItems()) > 0) {
          return super.beforeSave()
        }
        throw new Error('You cannot save invoice with no items')
      }
      throw new Error('Discount must be great or equal 0 and less or equal 100')
    }
    throw new Error("You don't assigned an customer")
  }

  async afterSave(invoice) {
    if (this.isNew()) {
      for (const id in this.state.items) {
        await this.saveItem(id)
      }
    }
  }

  getTotal() {
    let total = -1
    if (isObject(api.prices)) {
      total = 0
      const items = this.selectedItems()
      for (const id in items) {
        total += items[id] * api.prices[id]
      }
      const discount = +this.state.discount || 0
      if (discount > 0) {
        total = total * (100 - discount) / 100
      }
      total = Math.ceil(total * 100) / 100
    }
    return total
  }

  summary() {
    const total = this.getTotal()
    if (total >= 0) {
      return <div className="total">
        <span className="name">Total</span>
        <span className="value">{total}</span>
      </div>
    }
  }

  reset() {
    super.reset()
    this.setState({total: 0})
  }
}
