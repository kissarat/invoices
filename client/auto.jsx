import React, {Component} from 'react'
import api from './api.jsx'

export default class AutoComponent extends Component {
  state = {}

  collectionName(props = this.props) {
    return (props.params && props.params.collection) || this.constructor.collection
  }

  getURL(props = this.props) {
    let url = '/api/' + this.collectionName(props)
    if (props.params && props.params.id > 0) {
      url += '/' + props.params.id
    }
    return url
  }

  componentDidMount() {
    void this.load()
  }

  componentWillReceiveProps(props) {
    void this.load(props)
  }

  get model() {
    return api.getModel(this.collectionName())
  }
}
