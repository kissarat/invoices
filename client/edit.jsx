import api from './api.jsx'
import AutoComponent from './auto.jsx'
import React from 'react'
import {browserHistory} from 'react-router'
import {omit, clone} from 'lodash'

export default class Edit extends AutoComponent {
  reset(props = this.props) {
    const state = {error: ''}
    for (const name in this.model.fields) {
      state[name] = ''
    }
    try {
      const template = JSON.parse(localStorage.getItem(this.collectionName(props)))
      Object.assign(state, template)
    }
    catch (ex) {

    }
    this.setState(state)
  }

  async load(props = this.props) {
    if (this.isNew(props)) {
      this.reset(props)
    }
    else {
      const object = await api.fetch(this.getURL(props))
      if (object.id > 0) {
        object.error = ''
        this.setState(object)
        return object
      }
    }
  }

  get omited() {
    return omit(this.model.fields, 'id', 'createdAt', 'updatedAt')
  }

  fields() {
    const fields = []
    const omited = this.omited
    for (const name in omited) {
      const field = omited[name]
      fields.push(<div key={name} className="form-group">
        <label>{name}</label>
        <input
          className="form-control"
          name={name}
          type={'INTEGER' === field.type ? 'number' : 'text'}
          value={this.state[name] || ''}
          onChange={e => this.setState({[name]: e.target.value.trim()})}/>
      </div>)
    }
    return fields
  }

  name() {
    return (this.props.params.id > 0 ? 'Edit' : 'Create') + ' ' + api.getModelName(this.collectionName())
  }

  isNew(props = this.props) {
    return !(props.params.id > 0)
  }

  additional() {

  }

  getPersistent() {
    return omit(this.state, 'id', 'error', 'createdAt', 'updatedAt')
  }

  beforeSave() {
    return this.getPersistent()
  }

  afterSave() {

  }

  alert() {
    if (this.state.error) {
      return <div className="alert alert-danger" role="alert">{this.state.error}</div>
    }
  }

  summary() {

  }

  saveTemplate = () => {
    localStorage.setItem(this.collectionName(), JSON.stringify(this.getPersistent()))
  }

  resetTemplate = () => {
    localStorage.removeItem(this.collectionName())
    this.reset()
  }

  save = async() => {
    try {
      const body = await this.beforeSave()
      const r = await api.fetch(this.getURL(), {method: this.isNew() ? 'POST' : 'PUT', body})
      await this.afterSave(r)
      const name = this.collectionName()
      browserHistory.push(`/${name}/`)
    }
    catch (ex) {
      this.setState({error: ex.message || ex.toString()})
    }
  }

  render() {
    return <div className={'page edit ' + this.collectionName()}>
      <h1>{this.name()}</h1>
      {this.alert()}
      <fieldset>
        {this.fields()}
        {this.additional()}
      </fieldset>
      {this.summary()}
      <button type="button" className="btn btn-success" onClick={this.save}>
        {this.props.params.id > 0 ? 'Save' : 'Create'}
      </button>
      <button type="button" className="btn btn-primary" onClick={this.saveTemplate}>Save as Template</button>
      <button type="button" className="btn btn-danger" onClick={this.resetTemplate}>Reset</button>
    </div>
  }
}
