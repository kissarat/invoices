class API {
  async fetch(url, options) {
    if (options && options.body) {
      options.headers = {'Content-Type': 'application/json'}
      options.body = JSON.stringify(options.body)
    }
    const r = await fetch(url, options)
    return r.json()
  }

  async loadConfig() {
    this.config = await this.fetch('/api/config')
  }

  get schema() {
    return this.config.schema
  }

  getModelName(collection) {
    for (const name in this.schema) {
      const schema = this.schema[name]
      if (collection === schema.collection) {
        return name
      }
    }
  }

  getModel(collection) {
    return this.schema[this.getModelName(collection)]
  }

  async loadPrices(products) {
    if (!this.prices) {
      this.prices = {}
    }
    if (!(products instanceof Array)) {
      products = await this.fetch('/api/products')
    }
    if (products instanceof Array) {
      for(const product of products) {
        this.prices[product.id] = product.price
      }
    }
    return products
  }
}

const api = new API()
window.api = api

export default api
