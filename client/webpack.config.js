const webpack = require('webpack');
const package_json = require('../package.json')
const {map, pick} = require('lodash')

const mode = {}
for (const name of (process.env.PROJECT || '').split(',')) {
  mode[name] = true
}

const babel = {
  test: /\.jsx$/,
  exclude: /(node_modules)/,
  loader: 'babel-loader',
  query: {
    presets: [
      'react',
    ],
    plugins: [
      'transform-class-properties',
      'transform-object-rest-spread',
    ]
  }
}

const config = {
  entry: __dirname + '/main.jsx',
  output: {
    path: __dirname + '/../public/js',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      // {
      //   test: /\.css$/,
      //   use: [ 'style-loader', 'css-loader' ]
      // },
      babel
    ]
  },
  resolve: {},
  plugins: []
}

if (mode.dev) {
  config.devtool = 'source-map'
}

if (mode.usage) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
  config.plugins.push(new BundleAnalyzerPlugin({
    analyzerMode: 'server',
    analyzerHost: '127.0.0.1',
    analyzerPort: 8888,
  }))
}

if (mode.prod) {
  babel.query.presets.push(
    'es2015',
    'stage-1'
  )
  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      children: true,
      async: true,
    }),
    new webpack.optimize.UglifyJsPlugin({
      cacheFolder: '/tmp',
      debug: false,
      minimize: true,
      sourceMap: false,
      beautify: false,
      comments: false,
      compress: {
        sequences: true,
        booleans: true,
        loops: true,
        unused: true,
        warnings: false,
        drop_console: true,
        unsafe: true,
      },
      output: {
        comments: false
      }
    }),
    new webpack.BannerPlugin({
      banner: map(pick(package_json, 'name', 'version', 'url', 'email', 'author'), (v, k) => `@${k} ${v}`).join('\n'),
      entryOnly: false
    })
  )
}

module.exports = config
